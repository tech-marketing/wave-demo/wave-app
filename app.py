import streamlit as st
import time
from functions import sum


st.markdown("### Super Basic Calculator")

num_1 = st.number_input(label="Enter first number")
num_2 = st.number_input(label="Enter second number")


with st.spinner('Wait for it...'):
    time.sleep(0.4)
    answer = sum(num_1=num_1, num_2=num_2)
st.success(f"Answer = {answer}")
